import React from 'react';
import { Admin, Resource } from 'react-admin';
import Dashboard from './Items/Dashboard';
import { UserList } from './Items/Users';
import { TodoList, TodoEdit, TodoCreate} from './Items/Todos';
import { PostList, PostEdit, PostCreate } from './Items/Posts';
import jsonServerProvider from 'ra-data-json-server';
import PostIcon from '@material-ui/icons/Book';
import UserIcon from '@material-ui/icons/Group';
import TodoIcon from '@material-ui/icons/ListAlt';
import authProvider from './Auth/AuthProvider';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com');
const App = () => (
  <DndProvider backend={HTML5Backend}>
    <Admin dashboard={Dashboard} dataProvider={dataProvider} authProvider={authProvider} >
      <Resource name="todos" list={TodoList} edit={TodoEdit} create={TodoCreate} icon={TodoIcon}/>
      <Resource name="posts" list={PostList} edit={PostEdit} create={PostCreate} icon={PostIcon} />
      <Resource name="users" list={UserList} icon={UserIcon}/>
    </Admin>
  </DndProvider>
);

export default App;
