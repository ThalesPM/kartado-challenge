import React from 'react';
import {
  List, Create, Edit, SimpleForm, ReferenceInput, SelectInput, TextInput, Filter,
} from 'react-admin';
import { TasksGrid } from './Tasks';


export const TodoList = (props) => (
  <List
    {...props}
    title="To Do list"
    exporter={false}
    pagination={<></>}
    perPage={10}
    filters={<TodoFilter />}
    filterDefaultValues={{ userId: 1 }}
  >
    <TasksGrid />
  </List>
);

export const TodoCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="User" source="userId" reference="users">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="title" />
    </SimpleForm>
  </Create>
);

export const TodoEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <ReferenceInput label="User" source="userId" reference="users">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="title" />
    </SimpleForm>
  </Edit>
);

const TodoFilter = (props) => (
  <Filter {...props}>
    <ReferenceInput label="User" source="userId" reference="users" alwaysOn>
      <SelectInput optionText="name" />
    </ReferenceInput>
  </Filter>
);
