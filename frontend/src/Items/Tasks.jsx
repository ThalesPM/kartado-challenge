import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import PersonIcon from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import { TextField, DateField, ReferenceField, EditButton, useMutation } from "react-admin";
import { useDrag, useDrop } from 'react-dnd'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  card:{
    width: '95%',
    minHeight: 200,
    margin: '0.5em',
    display: 'inline-block',
    verticalAlign: 'top'
  },
  selected: {
    background: 'aliceblue',
  },
  notSelected:{
    background: 'white',
  },
  columnTitle:{
    textAlign: 'center',
  }
});

const TaskCard = ({ data, basePath, id }) => {
  const classes = useStyles();
  const [{ opacity }, dragRef] = useDrag({
    item: { type: "card", id },
    collect: (monitor) => ({
      opacity: monitor.isDragging() ? 0.5 : 1
    })
  });
  return (
    <div ref={dragRef} style={{ opacity }}>
      <Card key={id} className={classes.card}>
        <CardHeader
            title={
              <ReferenceField record={data[id]} basePath={basePath} source="userId" reference="users">
                <TextField record={data[id]} source="name" />
              </ReferenceField>
            }
            subheader={<DateField record={data[id]} source="created_at" />}
            avatar={<Avatar icon={<PersonIcon />} />}
        />
        <CardContent>
            <TextField record={data[id]} source="title" />
        </CardContent>
        <CardActions style={{ textAlign: 'right' }}>
            <EditButton resource="posts" basePath={basePath} record={data[id]} />
        </CardActions>
      </Card>
    </div>
  )

}

const DnDGrid = ({ children, title, mutate, data }) => {
  const classes = useStyles();

  const [{ isOver }, drop] = useDrop({
    accept: "card",
    drop: (item) => mutate({
      type: 'update',
      resource: 'todos',
      payload: { id: item.id, data: { ...data[item.id], completed: !data[item.id]?.completed } }
  }),
    collect: monitor => ({
      isOver: !!monitor.isOver(),
    }),
  })
  return (
    <Grid item xs={6} className={isOver? classes.selected: classes.notSelected} ref={drop}>
      <Card>
        <CardContent>
          <h3 className={classes.columnTitle}>{title}</h3>
        </CardContent>
      </Card>
        {children}
    </Grid>
  );

}

export const TasksGrid = ({ ids, data, basePath }) => {
  const [mutate] = useMutation();

  return (
    <Grid container>
      <DnDGrid title={"Task In Progress"} mutate={mutate} data={data}>
        {ids.filter((id)=> !data[id].completed).map(id => <TaskCard key={id} data={data} basePath={basePath} id={id}/>)}
      </DnDGrid>
      <DnDGrid title={"Task Completed"} mutate={mutate} data={data}>
        {ids.filter((id)=> data[id].completed).map(id => {
          console.log(data[id])
          return <TaskCard key={id} data={data} basePath={basePath} id={id}/>
        })}
      </DnDGrid>
    </Grid>
  );
}

TasksGrid.defaultProps = {
    data: {},
    ids: [],
};
